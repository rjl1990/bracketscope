'use strict';

import * as vscode from 'vscode';

export function activate(context: vscode.ExtensionContext) {

    console.log('Starting');

    const opening = new RegExp(/[\{\(\[]/, 'g');
    const closing = new RegExp(/[\}\)\]]/, 'g');
    const normb = new RegExp(/[\)\(]/);
    const curlyb = new RegExp(/[\}\{]/);
    const squareb = new RegExp(/[\[\]]/);
    
    const decorator = vscode.window.createTextEditorDecorationType({
        color: 'red'
    });
    
    const decorator1 = vscode.window.createTextEditorDecorationType({
        color: 'red'
	});

    let activeEditor = vscode.window.activeTextEditor;

vscode.window.onDidChangeTextEditorSelection(editor => {
    
            let doc = editor,
                start = doc.selections[0].start,
                data = doc.textEditor["_documentData"]; 
            // console.log("Main 1:",  doc);
            // console.log("Main 2:",  start);
            // console.log("Main 3:", lines);
           
            checkLines(data["_lines"], start.line);
            console.log(context.subscriptions)
    }, null, context.subscriptions);

    function makeArray (string) {
        let ret = string.split("");
        return ret;
    }

    function backLines (arr, BLine) {
        var line = BLine;
        for (let x = BLine + 1; x >= 0; x--) {
            if (opening.test(arr[x])) {
                line = [x].concat(findBracket(arr,x));
                break;
            }
        }
            return line
    }

    function forLines (arr, BLine) {
        var line = BLine;
        for (let w = BLine - 1; w <= arr.length; w++) {
            if (closing.test(arr[w])) {
                line = [w].concat(findBracket(arr,w));
                break;
            }
        }
        return line
    }

    function checkLines (arr, BLine) {
        console.log(BLine)
        let first = backLines(arr, BLine),
            last = forLines(arr, BLine)
        
        // Check if brackets are the same
        if (sameCheck(first[2],last[2]) == false) {

            if (first[0] == BLine) {
                last = forLines(arr, last[0] + 1);
            } else if (last[0] == BLine) {
                first = backLines(arr, first[0] - 1);
            }
        }

        var ret = [first, last];
        console.log(ret)

        let startd: vscode.DecorationOptions[] = [];
        let endd: vscode.DecorationOptions[] = [];

        let start = { range: new vscode.Range(new vscode.Position(first[0], first[1]), new vscode.Position(first[0], first[1] + 1))};
        startd.push(start);
        let startll = { range: new vscode.Range(new vscode.Position(last[0], last[1]), new vscode.Position(last[0], last[1] + 1))};
        endd.push(startll);

        activeEditor.setDecorations(decorator, startd);
        activeEditor.setDecorations(decorator1, endd);
        
        
    }

    function sameCheck (a,b) { // Acepts two bracket types
        let ret = false;
        var normCheck = normb.test(a) && normb.test(b),
            curlCheck = curlyb.test(a) && curlyb.test(b),
            squareCheck = squareb.test(a) && squareb.test(b);

        switch (true) {
            case normCheck:
                ret = true;
                break;
            case curlCheck:
                ret = true;
                break;
            case squareCheck:
                ret = true;
                break;
        }

        return ret;

    }

    function findBracket (arr, line) {
        let string = makeArray(arr[line]),
            set = [];
        for (let x = string.length; x >= 0; x--) {
            switch (string[x]) {
                case "}":
                    set = [x, string[x]]
                    break;
                case "{":
                    set = [x, string[x]]
                    break;
                case ")":
                    set = [x, string[x]]
                    break;
                case "(":
                    set = [x, string[x]]
                    break;
                case "]":
                    set = [x, string[x]]
                    break;
                case "[":
                    set = [x, string[x]]
                    break;
            }
        }
        return set
    }    
}

// this method is called when your extension is deactivated
export function deactivate() {
}